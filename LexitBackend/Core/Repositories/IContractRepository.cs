﻿using LexitBackend.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Core
{
    public interface IContractRepository
    {
        Task<Contract> GetContract(int id);
        Customer GetOwner(double customerNumber);
        Contract GetContractByCustomer(Customer customer);
        Contract GetContractByCustomerNumber(double customerNumber);
        void Add(Contract customer);
        void Remove(Contract contract);
    }
}
