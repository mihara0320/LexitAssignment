﻿using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Core.Repositories
{
    public interface IRecordRepository
    {
        Task<Record> GetRecord(int id);
        Task<IEnumerable<Record>> FindRecordsByMeterId(int meterId);
        void Add(Record record);
        void Remove(Record record);

    }
}
