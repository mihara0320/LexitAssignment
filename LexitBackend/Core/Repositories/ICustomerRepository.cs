﻿using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Core
{
    public interface ICustomerRepository
    {
        Task<Customer> GetCustomer(int id);
        Customer GetCustomerByCustomerNumber(double customerNumber);
        void Add(Customer customer);
        void Remove(Customer contract);
    }
}
