﻿using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Core.Repositories
{
    public interface IMeterRepository
    {
        Task<Meter> GetMeter(int id);
        Meter GetMeterByCustomerNumber(int customerNumber);
        Customer FindCustomer(double customerNumber);
        void Add(Meter meter);
        void Remove(Meter meter);
    }
}
