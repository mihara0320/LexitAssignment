﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Core.Models
{
    public class Contract
    {
        public int Id { get; set; }

        public int ContractNumber { get; set; }

        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        public DateTime ExpireDate { get; set; }

        public int BillingFrequency { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public DateTime LastUpdate { get; set; }

    }
}
