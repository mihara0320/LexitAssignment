﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Core.Models
{
    public class Record
    {
        public int Id { get; set; }

        public double Value { get; set; }

        public DateTime CollectionDate { get; set; }

        public DateTime LastUpdate { get; set; }

        public int MeterId { get; set; }
        public virtual Meter Meter { get; set; }
    }
}
