﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Core.Models
{
    public class Meter
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        public double InitialValue { get; set; }

        public string RegisterType { get; set; }

        public DateTime InstalledDate { get; set; }

        public DateTime LastUpdate { get; set; }

        public ICollection<Record> Records { get; set; }

        public Meter()
        {
            Records = new Collection<Record>();
        }

    }
}
