﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LexitBackend.Controllers.Resources;
using LexitBackend.Core.Models;
using LexitBackend.Mapping.Helpers;

namespace LexitBackend.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to API Resource   
            CreateMap<Customer, CustomerResource>()
                .ForSourceMember(c => c.Id, opt => opt.Ignore());

            CreateMap<Contract, ContractResource>()
                .ForSourceMember(c => c.Id, opt => opt.Ignore())
                .ForSourceMember(c => c.Customer, opt => opt.Ignore())
                .ForSourceMember(c => c.CustomerId, opt => opt.Ignore())
                .ForMember(cr => cr.CustomerNumber, opt => opt.MapFrom(c => c.Customer.CustomerNumber));

            CreateMap<Meter, MeterResource>()
                .ForSourceMember(m => m.Customer, opt => opt.Ignore())
                .ForSourceMember(m => m.CustomerId, opt => opt.Ignore())
                .ForSourceMember(m => m.Records, opt => opt.Ignore())
                .ForMember(mr => mr.CustomerNumber, opt => opt.MapFrom(m => m.Customer.CustomerNumber));

            // API Resource to Domain
            CreateMap<CustomerResource, Customer>();
            CreateMap<ContractResource, Contract>();
            CreateMap<MeterResource, Meter>();
            CreateMap<RecordResource, Record>();
        }
    }
}


