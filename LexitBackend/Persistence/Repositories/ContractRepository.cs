﻿using LexitBackend.Core;
using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Persistence
{
    public class ContractRepository: IContractRepository
    {
        private readonly AppDbContext context;

        public ContractRepository(AppDbContext context)
        {
            this.context = context;
        }
        public Customer GetOwner(double customerNumber)
        {
            return this.context.Customers.FirstOrDefault(c => c.CustomerNumber == customerNumber);
        }
        public Task<Contract> GetContract(int id)
        {
            return context.Contracts.FindAsync(id);
        }

        public Contract GetContractByCustomerNumber(double customerNumber)
        {
            return context.Contracts.FirstOrDefault(c => c.Customer.CustomerNumber == customerNumber);
        }

        public Contract GetContractByCustomer(Customer customer)
        {
            return context.Contracts.FirstOrDefault(c => c.Customer == customer);
        }
        public void Add(Contract customer)
        {
            context.Contracts.Add(customer);
        }

        public void Remove(Contract contract)
        {
            context.Remove(contract);
        }
    }
}
