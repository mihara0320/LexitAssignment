﻿using LexitBackend.Core;
using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Persistence
{
    public class CustomerRepository: ICustomerRepository
    {
        private readonly AppDbContext context;

        public CustomerRepository(AppDbContext context)
        {
            this.context = context;
        }

        public Task<Customer> GetCustomer(int id)
        {
            return context.Customers.FindAsync(id);
        }

        public Customer GetCustomerByCustomerNumber(double customerNumber)
        {
            return context.Customers.FirstOrDefault(c => c.CustomerNumber == customerNumber);
        }
        public void Add(Customer customer)
        {
            context.Customers.Add(customer);
        }

        public void Remove(Customer customer)
        {
            context.Remove(customer);
        }


    }
}
