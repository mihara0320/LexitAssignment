﻿using LexitBackend.Core.Models;
using LexitBackend.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace LexitBackend.Persistence.Repositories
{
    public class MeterRepository : IMeterRepository
    {
        private readonly AppDbContext context;

        public MeterRepository(AppDbContext context)
        {
            this.context = context;
        }
        public Meter GetMeterByCustomerNumber(int customerNumber)
        {
            var customer = context.Customers.Where(c => c.CustomerNumber == customerNumber).FirstOrDefault();
            return context.Meters.Where(m => m.CustomerId == customer.Id).FirstOrDefault();
        }
        public Customer FindCustomer(double customerNumber)
        {
            return context.Customers.FirstOrDefault(c => c.CustomerNumber == customerNumber);
        }
        public Task<Meter> GetMeter(int id)
        {
            return context.Meters.FindAsync(id);
        }
        public void Add(Meter meter)
        {
            context.Meters.Add(meter);
        }

        public void Remove(Meter meter)
        {
            context.Remove(meter);
        }
    }
}
