﻿using LexitBackend.Core.Models;
using LexitBackend.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace LexitBackend.Persistence.Repositories
{
    public class RecordRepository: IRecordRepository
    {
        private readonly AppDbContext context;

        public RecordRepository(AppDbContext context)
        {
            this.context = context;
        }
        public Task<Record> GetRecord(int id)
        {
            return context.Records.FindAsync(id);
        }
        public async Task<IEnumerable<Record>> FindRecordsByMeterId(int meterId)
        {
            return await context.Records.Where(record => record.MeterId == meterId).ToListAsync();
        }

        public void Add(Record record)
        {
            context.Records.Add(record);
        }
        public void Remove(Record record)
        {
            context.Remove(record);
        }


    }
}
