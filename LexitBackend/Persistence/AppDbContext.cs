﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LexitBackend.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace LexitBackend.Persistence
{
    public class AppDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Meter> Meters { get; set; }
        public DbSet<Record> Records { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
          : base(options)
        {
        }
    }
}
