﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LexitBackend.Core;

namespace LexitBackend.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext context;

        public UnitOfWork(AppDbContext context)
        {
            this.context = context;
        }

        public async Task CompleteAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}
