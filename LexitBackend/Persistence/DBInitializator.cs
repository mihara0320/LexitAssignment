﻿using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Persistence
{
    public class DBInitializator
    {
        public static void InitializeDBAsync(AppDbContext context)
        {
            if(!context.Customers.Any())
            {
                context.Customers.Add(new Customer()
                {
                    CustomerNumber = 56392,
                    FirstName = "Mike",
                    LastName = "Tyson",
                    Address = "Tallinn, Estonia",
                    Email = "mike@email.com",
                    LastUpdate = DateTime.Now
                });

                context.Customers.Add(new Customer()
                {
                    CustomerNumber = 85134,
                    FirstName = "John",
                    LastName = "Lennon",
                    Address = "Tallinn, Estonia",
                    Email = "john@email.com",
                    LastUpdate = DateTime.Now
                });

                context.Customers.Add(new Customer()
                {
                    CustomerNumber = 79634,
                    FirstName = "Michel",
                    LastName = "Jackson",
                    Address = "Tallinn, Estonia",
                    Email = "mj@email.com",
                    LastUpdate = DateTime.Now
                });
                context.SaveChanges();
            }

            if(!context.Contracts.Any())
            {
                context.Contracts.Add(new Contract()
                {
                    ContractNumber = 3197063,
                    CustomerId = 1,
                    ExpireDate = new DateTime(2018, 12, 31),
                    BillingFrequency = 1,
                    IsActive = true,
                    LastUpdate = DateTime.Now
                });
                context.SaveChanges();

                context.Contracts.Add(new Contract()
                {
                    ContractNumber = 9857458,
                    CustomerId = 2,
                    ExpireDate = new DateTime(2018, 06, 30),
                    BillingFrequency = 2,
                    IsActive = true,
                    LastUpdate = DateTime.Now
                });
                context.SaveChanges();

                context.Contracts.Add(new Contract()
                {
                    ContractNumber = 7845241,
                    CustomerId = 3,
                    ExpireDate = new DateTime(2018, 03, 30),
                    BillingFrequency = 1,
                    IsActive = false,
                    LastUpdate = new DateTime(2018, 03, 30)
                });
                context.SaveChanges();
            }


            if (!context.Meters.Any())
            {
                context.Meters.Add(new Meter()
                {
                    CustomerId = 1,
                    InitialValue = 500.50,
                    RegisterType = "mhw",
                    InstalledDate = new DateTime(2018, 1, 1),
                    LastUpdate = DateTime.Now
                });
                context.SaveChanges();

                context.Meters.Add(new Meter()
                {
                    CustomerId = 3,
                    InitialValue = 0.00,
                    RegisterType = "h",
                    InstalledDate = new DateTime(2018, 1, 1),
                    LastUpdate = new DateTime(2018, 3, 31)
                });
                context.SaveChanges();
            }

            if (!context.Records.Any())
            {
                context.Records.Add(new Record()
                {
                    Value = 1000.50,
                    CollectionDate = new DateTime(2018, 1, 1),
                    LastUpdate = DateTime.Now,
                    MeterId = 1
                });
                context.SaveChanges();

                context.Records.Add(new Record()
                {
                    Value = 1300.20,
                    CollectionDate = new DateTime(2018, 2, 1),
                    LastUpdate = DateTime.Now,
                    MeterId = 1
                });
                context.SaveChanges();

                context.Records.Add(new Record()
                {
                    Value = 2000.333,
                    CollectionDate = new DateTime(2018, 3, 1),
                    LastUpdate = DateTime.Now,
                    MeterId = 1
                });
                context.SaveChanges();

                context.Records.Add(new Record()
                {
                    Value = 3400,
                    CollectionDate = new DateTime(2018, 4, 1),
                    LastUpdate = DateTime.Now,
                    MeterId = 1
                });
                context.SaveChanges();

                context.Records.Add(new Record()
                {
                    Value = 300.50,
                    CollectionDate = new DateTime(2018, 1, 1),
                    LastUpdate = DateTime.Now,
                    MeterId = 2
                });
                context.SaveChanges();

                context.Records.Add(new Record()
                {
                    Value = 600,
                    CollectionDate = new DateTime(2018, 2, 1),
                    LastUpdate = DateTime.Now,
                    MeterId = 2
                });
                context.SaveChanges();

                context.Records.Add(new Record()
                {
                    Value = 900.333,
                    CollectionDate = new DateTime(2018, 3, 1),
                    LastUpdate = DateTime.Now,
                    MeterId = 2
                });
                context.SaveChanges();
            }
            
        }
    }

}
