﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LexitBackend.Controllers.Resources;
using LexitBackend.Core;
using LexitBackend.Core.Models;
using LexitBackend.Core.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LexitBackend.Controllers
{
    [Route("api/[controller]")]
    public class RecordController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IRecordRepository repository;
        private readonly IMapper mapper;

        public RecordController(IUnitOfWork unitOfWork, IRecordRepository repository, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet("{meterId}")]
        [Authorize]
        public async Task<IActionResult> GetRecordsByMeterId(int meterId)
        {
            var options = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            var records = await repository.FindRecordsByMeterId(meterId);
            if (records == null)
            {
                return BadRequest("Records Not Found");
            }

            return Json(records, options);
        }


        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateRecord([FromBody]RecordResource recordResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var record = mapper.Map<RecordResource, Record>(recordResource);
            record.CollectionDate = DateTime.Now;
            record.LastUpdate = DateTime.Now;
            repository.Add(record);
            await unitOfWork.CompleteAsync();

            record = await repository.GetRecord(record.Id);
            return Ok(record);
        }

    }
}
