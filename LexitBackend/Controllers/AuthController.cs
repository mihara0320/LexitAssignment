﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using LexitBackend.Controllers.Resources;
using LexitBackend.Core.Config;
using LexitBackend.Core.Models;
using LexitBackend.Persistence;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LexitBackend.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly AppDbContext context;
        private readonly IMapper mapper;

        public AuthController(AppDbContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetUserInfo()
        {
            var customerNumber = User.Identity.Name;

            var customer = context.Customers.Where(c => c.CustomerNumber == Int32.Parse(customerNumber)).FirstOrDefault();
            AuthResponse response = new AuthResponse();

            response.Customer = mapper.Map<Customer, CustomerResource>(customer);

            return Ok(response);
        }
        [HttpPost]
        public IActionResult Authenticate([FromBody]AuthResource authResource)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customerNumber = authResource.CustomerNumber;
            var contractNumber = authResource.ContractNumber;


            var customer = context.Customers.Where(c => c.CustomerNumber == customerNumber).FirstOrDefault();
            if (customer == null)
            {
                return NotFound($"Customer Not Found: {customerNumber}");
            }

            var contract = context.Contracts.Where(c => c.ContractNumber == contractNumber).FirstOrDefault();
            if (contract == null)
            {
                return NotFound($"Contract Not Found: {contractNumber}");
            }

            var claims = new[] { new Claim(ClaimTypes.Name, customerNumber.ToString()) };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtConfig.secret));
            var signInCred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);
            var token = new JwtSecurityToken(
                    issuer: JwtConfig.issuer,
                    audience: JwtConfig.audience,
                    expires: DateTime.Now.AddHours(1),
                    claims: claims,
                    signingCredentials: signInCred
                );
            var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            AuthResponse response = new AuthResponse();

            response.Token = tokenString;
            response.Customer = mapper.Map<Customer, CustomerResource>(customer);

            return Ok(response);
        }


    }
}
