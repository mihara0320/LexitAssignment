﻿using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Controllers.Resources
{
    public class ContractResource
    {
        public int ContractNumber { get; set; }

        public int CustomerNumber { get; set; }

        public DateTime ExpireDate { get; set; }

        public int BillingFrequency { get; set; }

        public bool IsActive { get; set; }

        public DateTime LastUpdate { get; set; }
    }
}
