﻿using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Controllers.Resources
{
    public class AuthResource
    {
        [Required]
        public int CustomerNumber { get; set; }
        [Required]
        public int ContractNumber { get; set; }
    }

    public class AuthResponse
    {
        public string Token { get; set; }

        public CustomerResource Customer { get; set; }
    }
}
