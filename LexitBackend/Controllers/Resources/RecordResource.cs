﻿using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Controllers.Resources
{
    public class RecordResource
    {
        public int Id { get; set; }

        [Required]
        public int MeterId { get; set; }

        public double Value { get; set; }

        public DateTime CollectionDate { get; set; }
    }
}
