﻿using LexitBackend.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LexitBackend.Controllers.Resources
{
    public class MeterResource
    {
        public int Id { get; set; }

        [Required]
        public int CustomerNumber { get; set; }

        public double InitialValue { get; set; }

        public string RegisterType { get; set; }

        public DateTime InstalledDate { get; set; }

        public DateTime LastUpdate { get; set; }
        
        public ICollection<Record> Records { get; set; }

        public MeterResource()
        {
            Records = new Collection<Record>();
        }

    }
}
