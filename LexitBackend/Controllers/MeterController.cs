﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LexitBackend.Controllers.Resources;
using LexitBackend.Core;
using LexitBackend.Core.Models;
using LexitBackend.Core.Repositories;
using LexitBackend.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LexitBackend.Controllers
{
    [Route("api/[controller]")]
    public class MeterController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMeterRepository repository;
        private readonly IMapper mapper;

        public MeterController(IUnitOfWork unitOfWork, IMeterRepository repository, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
        }

        [HttpGet("{customerNumber}")]
        [Authorize]
        public IActionResult GetMeter(int customerNumber)
        {
            var meter = repository.GetMeterByCustomerNumber(customerNumber);
 
            if (meter == null)
            {
                return NotFound($"Meter Not Found For CustomerNumber: {customerNumber}");
            }
            var result = mapper.Map<Meter, MeterResource>(meter);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> RegisterMeter([FromBody]MeterResource meterResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customer = repository.FindCustomer(meterResource.CustomerNumber);

            if(customer == null)
            {
                return NotFound($"Customer Not Found: {meterResource.CustomerNumber}");
            }

            var meter = mapper.Map<MeterResource, Meter>(meterResource);
            meter.Customer = customer;
            meter.InstalledDate = DateTime.Now;
            meter.LastUpdate = DateTime.Now;

            repository.Add(meter);
            await unitOfWork.CompleteAsync();

            meter = await repository.GetMeter(meter.Id);
            var result = mapper.Map<Meter, MeterResource>(meter);
            return Ok(result);
        }
    }
}
