﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LexitBackend.Controllers.Resources;
using LexitBackend.Core;
using LexitBackend.Core.Models;
using LexitBackend.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LexitBackend.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly ICustomerRepository repository;
        private readonly IMapper mapper;

        public CustomerController(ICustomerRepository repository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
        }

        //[Authorize]
        [HttpGet("{customerNumber}")]
        [Authorize]
        public IActionResult GetCustomer(int customerNumber)
        {
            var customer = repository.GetCustomerByCustomerNumber(customerNumber);
            if(customer == null)
            {
                return NotFound($"Customer Not Found: {customerNumber}");
            }
            return Ok(customer);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCustomer([FromBody]CustomerResource customerResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            var customer = mapper.Map<CustomerResource, Customer>(customerResource);
            customer.CustomerNumber = new Random().Next(10000, 100000);
            customer.LastUpdate = DateTime.Now;

            repository.Add(customer);
            await unitOfWork.CompleteAsync();

            customer = await repository.GetCustomer(customer.Id);
            var result = mapper.Map<Customer, CustomerResource>(customer);

            return Json(result);
        }

        [HttpPut("{customerNumber}")]
        [Authorize]
        public async Task<IActionResult> UpdateCustomer(int customerNumber, [FromBody]CustomerResource customerResource)
        {
            var customer = repository.GetCustomerByCustomerNumber(customerNumber);
            if (customer == null)
            {
                return NotFound($"Customer Not Found: {customerNumber}");
            }
            mapper.Map<CustomerResource, Customer>(customerResource, customer);
            customer.LastUpdate = DateTime.Now;

            await unitOfWork.CompleteAsync();

            customer = await repository.GetCustomer(customer.Id);
            var result = mapper.Map<Customer, CustomerResource>(customer);
            return Ok(result);
        }
    }
}
