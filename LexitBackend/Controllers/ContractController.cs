﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LexitBackend.Controllers.Resources;
using LexitBackend.Core;
using LexitBackend.Core.Models;
using LexitBackend.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LexitBackend.Controllers
{
    [Route("api/[controller]")]
    public class ContractController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IContractRepository repository;
        private readonly IMapper mapper;

        public ContractController(IContractRepository repository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
        }

        // GET api/<controller>/5
        [Authorize]
        [HttpGet("{customerNumber}")]
        public IActionResult GetContract(double customerNumber)
        {
            var contract = repository.GetContractByCustomerNumber(customerNumber);
            if (contract == null)
            {
                return NotFound($"Contract Not Found: {customerNumber}");
            }
            var result = mapper.Map<Contract, ContractResource>(contract);
            return Json(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateContract([FromBody]ContractResource contractResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var customer = repository.GetOwner(contractResource.CustomerNumber);

            var contract = mapper.Map<ContractResource, Contract>(contractResource);
            contract.Customer = customer;
            contract.ContractNumber = new Random().Next(1000000, 10000000);
            contract.LastUpdate = DateTime.Now;

            repository.Add(contract);
            await unitOfWork.CompleteAsync();

            contract = await repository.GetContract(contract.Id);
            var result = mapper.Map<Contract, ContractResource>(contract);
            return Ok(result);
        }

    }
}
