import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map, tap } from 'rxjs/operators';

import {
  AuthActionTypes,
  Login,
  LoginFailure,
  LoginSuccess,
  UpdateCustomer,
  UpdateCustomerComplete
} from '../actions/auth.actions';
import { Authenticate } from '../models/auth';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from '@core/services/customer.service';

@Injectable()
export class AuthEffects {
  @Effect()
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.Login),
    map(action => action.payload),
    exhaustMap((auth: Authenticate) =>
      this.authService
        .login(auth)
        .pipe(
          tap(res => {
            localStorage.setItem('token', res.token);
            this.toastr.success('Logged In', 'Success');
          }),
          map(res => new LoginSuccess(res)),
          catchError(error => {
            console.log(error);
            return of(new LoginFailure(error));
          })
        )
    )
  );

  @Effect()
  autoLogin$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.AutoLogin),
    exhaustMap(() =>
      this.authService
        .retriveCustomerNumber()
        .pipe(
          tap(res => this.toastr.success('Logged In', 'Success')),
          map(res => new LoginSuccess(res)),
          catchError(error => {
            console.log(error);
            return of(new LoginFailure(error));
          })
        )
    )
  );

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginSuccess),
    tap(() => this.router.navigate(['/']))
  );


  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$.pipe(
    ofType(AuthActionTypes.LoginRedirect),
    tap(authed => {
      this.router.navigate(['/login']);
    })
  );

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(AuthActionTypes.Logout),
    tap(authed => {
      this.authService.logout();
      this.toastr.success('Logged Out', 'Success');
      this.router.navigate(['/login']);
    })
  );

  @Effect()
  updateCustomer$ = this.actions$.pipe(
    ofType<UpdateCustomer>(AuthActionTypes.UpdateCustomer),
    map(action => action.payload),
    exhaustMap(customer => this.customerService.updateCustomer(customer)
      .pipe(
        map((res) => new UpdateCustomerComplete(res))
      )
    )
  );


  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private customerService: CustomerService
  ) { }
}
