import { Component, OnInit, AfterContentInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Authenticate } from '@auth/models/auth';
import * as fromAuth from '@auth/reducers';
import * as AuthActions from '@auth/actions/auth.actions';
import {
  trigger,
  style,
  transition,
  animate,
  query,
} from '@angular/animations';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  animations: [
    trigger('animState', [
      transition('* => in', [
        query('.mat-card', [
          style({ opacity: 0 }),
          animate('750ms ease-in-out', style({ opacity: 1 }))
        ]),
      ])
    ])
  ]
})
export class LoginPageComponent implements OnInit, AfterContentInit {
  pending$ = this.store.pipe(select(fromAuth.getLoginPagePending));
  error$ = this.store.pipe(select(fromAuth.getLoginPageError));
  animState;
  constructor(
    private store: Store<fromAuth.State>
  ) { }

  ngOnInit() {
    if (localStorage.getItem('token')) {
      this.store.dispatch(new AuthActions.AutoLogin());
    }
  }

  ngAfterContentInit() {
    this.animState = 'in';
  }

  onSubmit($event: Authenticate) {
    this.store.dispatch(new AuthActions.Login($event));
  }
}
