import { AuthActionsUnion, AuthActionTypes } from './../actions/auth.actions';
import { Customer, Contract } from '@core/models/summary';

export interface State {
  loggedIn: boolean;
  customer: Customer | null;
}

export const initialState: State = {
  loggedIn: false,
  customer: null
};

export function reducer(state = initialState, action: AuthActionsUnion): State {
  switch (action.type) {
    case AuthActionTypes.LoginSuccess: {
      return {
        ...state,
        loggedIn: true,
        customer: action.payload.customer
      };
    }
    case AuthActionTypes.UpdateCustomerComplete: {
      return {
        ...state,
        customer: action.payload
      };
    }
    case AuthActionTypes.Logout: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}

export const getAll = (state: State) => state;
export const getLoggedIn = (state: State) => state.loggedIn;
export const getCustomer = (state: State) => state.customer;

