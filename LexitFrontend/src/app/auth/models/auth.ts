import { Customer, Contract } from '@core/models/summary';

export interface Authenticate {
    customerNumber: number;
    contractNumber: number;
}

export interface AuthResponse {
    token?: string;
    customer: Customer;
}
