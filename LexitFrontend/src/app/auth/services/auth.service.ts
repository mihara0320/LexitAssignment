import { Injectable } from '@angular/core';
import { HttpClient, } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api } from '@config/app.config';
import { Authenticate, AuthResponse } from '@auth/models/auth';


@Injectable()
export class AuthService {

  constructor(
    private _http: HttpClient,
  ) { }
  login({ customerNumber, contractNumber }: Authenticate): Observable<AuthResponse> {
    const endpoint = api.auth;
    const payload = { CustomerNumber: customerNumber, ContractNumber: contractNumber };
    return this._http
      .post<AuthResponse>(endpoint, payload);
  }
  retriveCustomerNumber() {
    const endpoint = api.auth;
    return this._http
      .get<any>(endpoint);
  }
  logout() {
    localStorage.removeItem('token');
  }
}
