import { Action } from '@ngrx/store';
import { Authenticate, AuthResponse } from '@auth/models/auth';
import { Customer } from '@core/models/summary';

export enum AuthActionTypes {
  Login = '[Auth] Login',
  Logout = '[Auth] Logout',
  AutoLogin = '[Auth] AutoLogin',
  LoginSuccess = '[Auth] Login Success',
  LoginFailure = '[Auth] Login Failure',
  LoginRedirect = '[Auth] Login Redirect',
  UpdateCustomer = '[Auth] Update Customer',
  UpdateCustomerComplete = '[Auth] Update Customer Complete',
}

export class Login implements Action {
  readonly type = AuthActionTypes.Login;
  constructor(public payload: Authenticate) {}
}

export class AutoLogin implements Action {
  readonly type = AuthActionTypes.AutoLogin;
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;
  constructor(public payload: AuthResponse) {}
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure;
  constructor(public payload: any) {}
}

export class LoginRedirect implements Action {
  readonly type = AuthActionTypes.LoginRedirect;
}

export class Logout implements Action {
  readonly type = AuthActionTypes.Logout;
}

export class UpdateCustomer implements Action {
  readonly type = AuthActionTypes.UpdateCustomer;
  constructor(public payload: Customer) { }
}

export class UpdateCustomerComplete implements Action {
  readonly type = AuthActionTypes.UpdateCustomerComplete;
  constructor(public payload: Customer) { }
}

export type AuthActionsUnion =
  | Login
  | LoginSuccess
  | LoginFailure
  | LoginRedirect
  | Logout
  | UpdateCustomer
  | UpdateCustomerComplete;
