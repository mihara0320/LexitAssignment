const ENDPOINTS = {
    auth: '/api/auth',
    customer: '/api/customer',
    contract: '/api/contract',
    meter: '/api/meter',
    record: '/api/record',
};

export class Endpoint {
    private _api: string;
    private readonly _auth = ENDPOINTS.auth;
    private readonly _customer = ENDPOINTS.customer;
    private readonly _contract = ENDPOINTS.contract;
    private readonly _meter = ENDPOINTS.meter;
    private readonly _record = ENDPOINTS.record;

    constructor(api: string) {
        this._api = api;
    }

    get auth(): string { return this._api + this._auth; }
    get customer(): string { return this._api + this._customer; }
    get contract(): string { return this._api + this._contract; }
    get meter(): string { return this._api + this._meter; }
    get record(): string { return this._api + this._record; }
}
