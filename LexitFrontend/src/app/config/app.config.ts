import { InjectionToken } from '@angular/core';
import { environment } from '@environments/environment';
import { IAppConfig } from './iapp.config';
import { Links } from './links.config';
import { Endpoint } from './endpoint.config';

export const APP_CONFIG = new InjectionToken('app.config');
export const api = new Endpoint(environment.api);
export const links = new Links(environment.production);

export const AppConfig: IAppConfig = {
    routes: {
        error404: '404'
    },
};
