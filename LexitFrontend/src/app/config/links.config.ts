import * as _ from 'lodash';

export class Links {
    private _activeLinks: any[];
    private _prod: boolean;
    private _links: any[] = [
        { label: 'SUMMARY', path: 'overview/summary', prod: true },
        { label: 'METER', path: 'overview/meter', prod: true },
    ];

    constructor(prod: boolean) {
        this._prod = prod;
    }

    get activeLinks(): any {
        return this._prod ? _.filter(this._links, ['prod', true]) : this._links;
    }
}
