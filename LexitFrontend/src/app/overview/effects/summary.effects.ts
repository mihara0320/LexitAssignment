import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
    map,
    exhaustMap
} from 'rxjs/operators';

import {
    SummaryActionTypes,
    LoadContract,
    LoadContractComplete
} from '../actions/summary.actions';

import { CustomerService } from '@core/services/customer.service';

@Injectable()
export class SummaryEffects {
    @Effect()
    loadContract$: Observable<Action> = this.actions$.pipe(
        ofType<LoadContract>(SummaryActionTypes.LoadContract),
        map(action => action.payload),
        exhaustMap(customerNumber => this.customerService.getContract(customerNumber)
            .pipe(
                map((res) => new LoadContractComplete(res))
            )
        )
    );


    constructor(
        private actions$: Actions,
        private customerService: CustomerService,
    ) { }
}
