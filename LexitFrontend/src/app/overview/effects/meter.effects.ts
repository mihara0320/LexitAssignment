import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
    map,
    exhaustMap
} from 'rxjs/operators';

import {
    MeterActionTypes,
    LoadMeter,
    LoadMeterComplete,
    LoadRecords,
    LoadRecordsComplete,
    AddMeter,
    AddMeterComplete,
    AddRecords,
    AddRecordsComplete
} from '../actions/meter.actions';

import { MeterService } from '@core/services/meter.service';

@Injectable()
export class MeterEffects {
    @Effect()
    loadMeter$: Observable<Action> = this.actions$.pipe(
        ofType<LoadMeter>(MeterActionTypes.LoadMeter),
        map(action => action.payload),
        exhaustMap(customerNumber => this.meterService.getMeter(customerNumber)
            .pipe(
                map((res) => new LoadMeterComplete(res))
            )
        )
    );

    @Effect()
    addMeter$: Observable<Action> = this.actions$.pipe(
        ofType<AddMeter>(MeterActionTypes.AddMeter),
        map(action => action.payload),
        exhaustMap(registerMeter => this.meterService.registerMeter(registerMeter)
            .pipe(
                map((res) => new AddMeterComplete(res))
            )
        )
    );

    @Effect()
    loadRecords$: Observable<Action> = this.actions$.pipe(
        ofType<LoadRecords>(MeterActionTypes.LoadRecords),
        map(action => action.payload),
        exhaustMap(meterId => this.meterService.getRecords(meterId)
            .pipe(
                map((res) => new LoadRecordsComplete(res))
            )
        )
    );

    @Effect()
    addRecord$: Observable<Action> = this.actions$.pipe(
        ofType<AddRecords>(MeterActionTypes.AddRecords),
        map(action => action.payload),
        exhaustMap(registerRecord => this.meterService.registerRecord(registerRecord)
            .pipe(
                map((res) => new AddRecordsComplete(res))
            )
        )
    );

    @Effect()
    addRecordsComplete$: Observable<Action> = this.actions$.pipe(
        ofType<AddRecordsComplete>(MeterActionTypes.AddRecordsComplete),
        map(action => action.payload),
        exhaustMap(record => this.meterService.getRecords(record.meterId)
            .pipe(
                map((res) => new LoadRecordsComplete(res))
            )
        )
    );
    constructor(
        private actions$: Actions,
        private meterService: MeterService,
    ) { }
}
