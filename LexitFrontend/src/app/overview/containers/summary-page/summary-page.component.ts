import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Store, select } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import * as fromRoot from '../../../reducers';
import * as fromOverview from '@overview/reducers';
import * as SummaryActions from '@overview/actions/summary.actions';
import * as AuthActions from '@auth/actions/auth.actions';
import { Page } from '@classes/Page/class';
import { Customer, Contract } from '@core/models/summary';
import { EmailDialogComponent } from '@core/components/email-dialog/email-dialog.component';

import * as _ from 'lodash';

@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrls: ['./summary-page.component.scss']
})
export class SummaryPageComponent extends Page implements OnInit {
  contract$: Observable<Contract>;

  customer: Customer;
  contract: Contract;

  constructor(
    _store: Store<fromRoot.State>,
    public dialog: MatDialog
  ) {
    super(_store);
    this.contract$ = this.store.pipe(select(fromOverview.getContract));
  }

  ngOnInit() {
    const customer$ = this.customer$.subscribe(customer => {
      if (customer) {
        this.customer = customer;
        this.loadContract();
      }
    });
    this.subscriptions.push(customer$);

    const contract$ = this.contract$.subscribe(contract => {
      if (contract) {
        this.contract = contract;
        this.loadContract();
      }
    });
    this.subscriptions.push(customer$);

  }

  loadContract() {
    this.store.dispatch(new SummaryActions.LoadContract(this.customer.customerNumber));
  }
  openEmailDialog(): void {
    const dialogRef = this.dialog.open(EmailDialogComponent, {
      width: '50vw',
      disableClose: true,
      data: {
        title: 'Change Email',
        actionType: 'Change',
      }
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result) {
        const email = result.email;
        const customer = _.clone(this.customer);
        customer.email = email;
        this.store.dispatch(new AuthActions.UpdateCustomer(customer));
      }
    });
  }
}
