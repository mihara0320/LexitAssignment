import { Component, OnInit, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import * as fromOverview from '@overview/reducers';
// import * as fromMeterPage from '@overview/reducers/meter-page.reducer';
import * as MeterActions from '@overview/actions/meter.actions';

import { Page } from '@classes/Page/class';
import { Customer, Contract } from '@core/models/summary';
import { Meter, Record, RegisterMeter, RegisterRecord } from '@core/models/meter';
import { LineChartItem } from '@shared/components/line-chart/line-chart.component';
import { MeterDialogComponent } from '@overview/components/meter-dialog/meter-dialog.component';
import { RecordDialogComponent } from '@overview/components/record-dialog/record-dialog.component';

const REGISTER_TYPES = ['MWh', 'M3', 'Hour'];

@Component({
  selector: 'app-meter-page',
  templateUrl: './meter-page.component.html',
  styleUrls: ['./meter-page.component.scss']
})
export class MeterPageComponent extends Page implements OnInit {
  meter$: Observable<Meter>;
  records$: Observable<Record[]>;
  contract$: Observable<Contract>;

  customer: Customer;
  meter: Meter;
  contractIsActive: boolean;
  registerType: string;

  records_chart$ = new EventEmitter<LineChartItem[]>();
  records_pending = new EventEmitter<boolean>();
  records_underflow = new EventEmitter<string>();

  constructor(
    _store: Store<fromRoot.State>,
    public dialog: MatDialog
  ) {
    super(_store);
    this.meter$ = this.store.pipe(select(fromOverview.getMeter));
    this.records$ = this.store.pipe(select(fromOverview.getRecord));
    this.contract$ = this.store.pipe(select(fromOverview.getContract));
    // this.page$ = this.store.pipe(select(fromOverview.getMeterPage));
  }

  ngOnInit() {
    const customer$ = this.customer$.subscribe(customer => {
      if (customer) {
        this.customer = customer;
        this.loadMeter();
      }
    });
    this.subscriptions.push(customer$);

    const meter$ = this.meter$.subscribe(meter => {
      if (meter) {
        this.meter = meter;
        this.registerType = this.meter.registerType;
        this.loadRecords();
      }
    });
    this.subscriptions.push(meter$);

    const records$ = this.records$.subscribe(records => {
      if (records) {
        this.handleRecords(records);
      }
    });
    this.subscriptions.push(records$);

    const contract$ = this.contract$.subscribe(contract => {
      if (contract) {
        this.contractIsActive = contract.isActive;
      }
    });
    this.subscriptions.push(customer$);
  }
  loadMeter() {
    if (this.customer) {
      this.store.dispatch(new MeterActions.LoadMeter(this.customer.customerNumber));
    }
  }
  loadRecords() {
    if (this.meter) {
      this.store.dispatch(new MeterActions.LoadRecords(this.meter.id));
    }
  }
  handleRecords(data: Record[]) {
    const items: LineChartItem[] = [];
    data.forEach(record => {
      const arr = []; // format -> [timestamp, value]
      const yyyy = record.CollectionDate.substring(0, 4);
      const mm = record.CollectionDate.substring(5, 7);
      const dd = record.CollectionDate.substring(8, 10);
      const timestamp = Math.round(new Date(`${yyyy}/${mm}/${dd}`).getTime());

      arr.push(timestamp);
      arr.push(record.Value);
      items.push(arr);
    });
    this.records_chart$.emit(items);
  }
  openMeterDialog(): void {
    const dialogRef = this.dialog.open(MeterDialogComponent, {
      width: '50vw',
      disableClose: true,
      data: {
        title: 'Register Meter',
        actionType: 'Register',
        registerTypes: REGISTER_TYPES
      }
    });
    dialogRef.afterClosed().subscribe((result: RegisterMeter) => {
      if (result) {
        result.customerNumber = this.customer.customerNumber;
        this.store.dispatch(new MeterActions.AddMeter(result));
      }
    });
  }
  openRecordDialog(): void {
    const dialogRef = this.dialog.open(RecordDialogComponent, {
      width: '50vw',
      disableClose: true,
      data: {
        title: 'Add Record',
        actionType: 'Add',
      }
    });
    dialogRef.afterClosed().subscribe((result: RegisterRecord) => {
      if (result) {
        result.meterId = this.meter.id;
        this.store.dispatch(new MeterActions.AddRecords(result));
      }
    });
  }
}
