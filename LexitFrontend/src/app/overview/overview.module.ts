import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { OverviewComponent } from './containers/overview/overview.component';
import { MeterPageComponent } from './containers/meter-page/meter-page.component';
import { SummaryPageComponent } from './containers/summary-page/summary-page.component';

import { SharedModule } from '@shared/shared.module';
import { RoutesModule } from './overview.routes';

import { reducers } from './reducers';
import { MeterEffects } from '@overview/effects/meter.effects';
import { SummaryEffects } from '@overview/effects/summary.effects';

import { ComponentsModule } from './components/components.module';

@NgModule({
    declarations: [
        OverviewComponent,
        SummaryPageComponent,
        MeterPageComponent],
    imports: [
        CommonModule,
        SharedModule,
        RoutesModule,
        ComponentsModule,
        StoreModule.forFeature('overview', reducers),
        EffectsModule.forFeature([MeterEffects, SummaryEffects]),
    ],
})
export class OverviewModule { }
