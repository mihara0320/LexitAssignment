
import { Action } from '@ngrx/store';
import { Meter, Record, RegisterRecord, RegisterMeter } from '@core/models/meter';

export enum MeterActionTypes {
    LoadMeter = '[Meter] LoadMeter',
    LoadMeterComplete = '[Meter] LoadMeter Complete',
    AddMeter = '[Meter] AddMeter',
    AddMeterComplete = '[Meter] AddMeter Complete',

    LoadRecords = '[Meter] LoadRecords',
    LoadRecordsComplete = '[Meter] LoadRecords Complete',
    AddRecords = '[Meter] AddRecords',
    AddRecordsComplete = '[Meter] AddRecords Complete',
}

export class LoadMeter implements Action {
    readonly type = MeterActionTypes.LoadMeter;
    constructor(public payload: number) { }
}
export class LoadMeterComplete implements Action {
    readonly type = MeterActionTypes.LoadMeterComplete;
    constructor(public payload: Meter) { }
}
export class AddMeter implements Action {
    readonly type = MeterActionTypes.AddMeter;
    constructor(public payload: RegisterMeter) { }
}
export class AddMeterComplete implements Action {
    readonly type = MeterActionTypes.AddMeterComplete;
    constructor(public payload: any) { }
}

export class LoadRecords implements Action {
    readonly type = MeterActionTypes.LoadRecords;
    constructor(public payload: number) { }
}
export class LoadRecordsComplete implements Action {
    readonly type = MeterActionTypes.LoadRecordsComplete;
    constructor(public payload: Record[]) { }
}
export class AddRecords implements Action {
    readonly type = MeterActionTypes.AddRecords;
    constructor(public payload: RegisterRecord) { }
}
export class AddRecordsComplete implements Action {
    readonly type = MeterActionTypes.AddRecordsComplete;
    constructor(public payload: any) { }
}

export type MeterActionsUnion =
    | LoadMeter
    | LoadMeterComplete
    | AddMeter
    | AddMeterComplete
    | LoadRecords
    | LoadRecordsComplete
    | AddRecords
    | AddRecordsComplete;
