
import { Action } from '@ngrx/store';
import { Contract } from '@core/models/summary';

export enum SummaryActionTypes {
    LoadContract = '[Summary] LoadMeter',
    LoadContractComplete = '[Summary] LoadMeter Complete',

    UpdateCustomer = '[Summary] UpdateCustomer',
    UpdateCustomerComplete = '[Summary] UpdateCustomer Complete',
}

export class LoadContract implements Action {
    readonly type = SummaryActionTypes.LoadContract;
    constructor(public payload: number) { }
}
export class LoadContractComplete implements Action {
    readonly type = SummaryActionTypes.LoadContractComplete;
    constructor(public payload: Contract) { }
}


export type SummaryActionsUnion =
    | LoadContract
    | LoadContractComplete;
