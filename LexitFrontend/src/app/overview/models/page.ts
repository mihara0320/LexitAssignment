export interface PageState {
    error: string | null;
    pending: boolean;
}
