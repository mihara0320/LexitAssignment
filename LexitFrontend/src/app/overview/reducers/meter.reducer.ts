import { MeterActionsUnion, MeterActionTypes } from '../actions/meter.actions';
import { Meter, Record } from '@core/models/meter';

export interface State {
    meter: Meter | null;
    records: Record[] | null;
}

export const initialState: State = {
    meter: null,
    records: null,
};

export function reducer(state = initialState, action: MeterActionsUnion): State {
    switch (action.type) {
        case MeterActionTypes.LoadMeterComplete: {
            return {
                ...state,
                meter: action.payload,
            };
        }
        case MeterActionTypes.AddMeterComplete: {
            return {
                ...state,
                meter: action.payload,
            };
        }
        case MeterActionTypes.LoadRecordsComplete: {
            return {
                ...state,
                records: action.payload,
            };
        }
        default: {
            return state;
        }
    }
}

export const getMeter = (state: State) => state.meter;
export const getRecord = (state: State) => state.records;
