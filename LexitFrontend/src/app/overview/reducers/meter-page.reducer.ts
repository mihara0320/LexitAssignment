import { MeterActionsUnion, MeterActionTypes } from '../actions/meter.actions';
import { PageState } from '@overview/models/page';

export interface State {
    meter: PageState;
}

export const initialState: State = {
    meter: {
        error: null,
        pending: false,
    }
};

export function reducer(state = initialState, action: MeterActionsUnion): State {
    switch (action.type) {
        case MeterActionTypes.LoadMeter: {
            return {
                ...state,
                meter: {
                    error: null,
                    pending: true,
                }
            };
        }
        case MeterActionTypes.LoadMeterComplete: {
            return {
                ...state,
                meter: {
                    error: null,
                    pending: false,
                }
            };
        }
        default: {
            return state;
        }
    }
}

