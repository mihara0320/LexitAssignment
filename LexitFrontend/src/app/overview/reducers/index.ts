import {
    createSelector,
    createFeatureSelector,
    ActionReducerMap,
} from '@ngrx/store';
import * as fromMeter from './meter.reducer';
import * as fromMeterPage from './meter-page.reducer';

import * as fromSummary from './summary.reducer';

import * as fromRoot from '../../reducers';

export interface OverviewState {
    meter: fromMeter.State;
    meterPage: fromMeterPage.State;
    summary: fromSummary.State;
}

export interface State extends fromRoot.State {
    overview: OverviewState;
}

export const reducers: ActionReducerMap<OverviewState> = {
    meter: fromMeter.reducer,
    meterPage: fromMeterPage.reducer,
    summary: fromSummary.reducer
};

export const selectOverviewState = createFeatureSelector<OverviewState>('overview');

export const selectMeter = createSelector(selectOverviewState, (state: OverviewState) => state.meter);
export const selectMeterPage = createSelector(selectOverviewState, (state: OverviewState) => state.meterPage);

export const getMeter = createSelector(selectMeter, fromMeter.getMeter);
export const getRecord = createSelector(selectMeter, fromMeter.getRecord);

export const selectSummary = createSelector(selectOverviewState, (state: OverviewState) => state.summary);
export const getContract = createSelector(selectSummary, fromSummary.getContract);
