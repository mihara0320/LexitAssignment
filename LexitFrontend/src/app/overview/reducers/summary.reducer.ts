import { SummaryActionsUnion, SummaryActionTypes } from '../actions/summary.actions';
import { Contract, Customer } from '@core/models/summary';

export interface State {
    contract: Contract | null;
}

export const initialState: State = {
    contract: null,
};

export function reducer(state = initialState, action: SummaryActionsUnion): State {
    switch (action.type) {
        case SummaryActionTypes.LoadContractComplete: {
            return {
                ...state,
                contract: action.payload,
            };
        }
        default: {
            return state;
        }
    }
}

export const getContract = (state: State) => state.contract;
