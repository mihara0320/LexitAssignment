import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OverviewComponent } from './containers/overview/overview.component';
import { SummaryPageComponent } from './containers/summary-page/summary-page.component';
import { MeterPageComponent } from './containers/meter-page/meter-page.component';


const routes: Routes = [
    {
        path: '',
        component: OverviewComponent,
        children: [
            { path: '', redirectTo: 'summary', pathMatch: 'full' },
            { path: 'summary', component: SummaryPageComponent },
            { path: 'meter', component: MeterPageComponent },
        ]
    }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RoutesModule { }
