import { Component, Input } from '@angular/core';
import { Meter } from '@core/models/meter';

@Component({
  selector: 'app-meter-info',
  templateUrl: './meter-info.component.html',
  styleUrls: ['./meter-info.component.scss']
})
export class MeterInfoComponent {
  @Input('meter') meter: Meter;
  @Input('registerType') registerType: string;
}
