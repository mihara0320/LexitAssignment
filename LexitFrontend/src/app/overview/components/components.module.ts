import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExternalModule } from '@shared/externals/external.module';
import * as SharedComponents from '@shared/components/components.module';
import { MeterDialogComponent } from '@overview/components/meter-dialog/meter-dialog.component';
import { RecordDialogComponent } from '@overview/components/record-dialog/record-dialog.component';
import { CustomerInfoComponent } from '@overview/components/customer-info/customer-info.component';

import { PipesModule } from '@shared/pipes/pipe.module';
import { ContractInfoComponent } from '@overview/components/contract-info/contract-info.component';
import { MeterInfoComponent } from '@overview/components/meter-info/meter-info.component';
import { RecordInfoComponent } from '@overview/components/record-info/record-info.component';


export const COMPONENTS = [
    MeterDialogComponent,
    RecordDialogComponent,
    CustomerInfoComponent,
    ContractInfoComponent,
    MeterInfoComponent,
    RecordInfoComponent
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ExternalModule,
        SharedComponents.ComponentsModule,
        PipesModule
    ],
    entryComponents: [
        MeterDialogComponent,
        RecordDialogComponent
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class ComponentsModule { }
