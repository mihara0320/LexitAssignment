import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  Validators
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSlideToggle, MatSlideToggleChange } from '@angular/material';
import { Dialog } from '@classes/Dialog/class';

@Component({
  selector: 'app-record-dialog',
  templateUrl: './record-dialog.component.html',
  styleUrls: ['./record-dialog.component.scss']
})
export class RecordDialogComponent extends Dialog implements OnInit {
  @ViewChild('toggle') toggle: MatSlideToggle;
  title: string;
  actionType: string;

  constructor(
    private fb: FormBuilder,
    public dRef: MatDialogRef<RecordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(fb, dRef);
    this.buildFormGroup({
      value: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.title = this.data.title;
    this.actionType = this.data.actionType;
  }
}
