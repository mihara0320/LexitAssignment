import { Component, Input, EventEmitter } from '@angular/core';
import { LineChartItem } from '@shared/components/line-chart/line-chart.component';

@Component({
  selector: 'app-record-info',
  templateUrl: './record-info.component.html',
  styleUrls: ['./record-info.component.scss']
})
export class RecordInfoComponent {
  @Input('dataEmitter') dataEmitter: EventEmitter<LineChartItem[]>;
  @Input('registerType') registerType: string;
}
