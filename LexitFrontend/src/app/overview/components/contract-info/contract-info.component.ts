import { Component, Input } from '@angular/core';
import { Contract } from '@core/models/summary';

@Component({
  selector: 'app-contract-info',
  templateUrl: './contract-info.component.html',
  styleUrls: ['./contract-info.component.scss']
})
export class ContractInfoComponent {
  @Input('contract') contract: Contract;
}
