import { Component, Input } from '@angular/core';
import { Customer } from '@core/models/summary';

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.scss']
})
export class CustomerInfoComponent {
  @Input('customer') customer: Customer;
}
