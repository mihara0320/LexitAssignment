import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Dialog } from '@classes/Dialog/class';

@Component({
  selector: 'app-meter-dialog',
  templateUrl: './meter-dialog.component.html',
  styleUrls: ['./meter-dialog.component.scss']
})
export class MeterDialogComponent extends Dialog implements OnInit {
  title: string;
  actionType: string;
  registerTypes: any;
  constructor(
    private fb: FormBuilder,
    public dRef: MatDialogRef<MeterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(fb, dRef);
    this.buildFormGroup({
      initialValue: [null, [Validators.required]],
      registerType: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.title = this.data.title;
    this.actionType = this.data.actionType;
    this.registerTypes = this.data.registerTypes;
  }
}
