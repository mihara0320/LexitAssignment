import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeterDialogComponent } from './meter-dialog.component';

describe('MeterDialogComponent', () => {
  let component: MeterDialogComponent;
  let fixture: ComponentFixture<MeterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
