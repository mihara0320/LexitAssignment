import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundPageComponent } from '@core/containers/not-found-page/not-found-page.component';
import { AuthGuard } from '@auth/services/auth-guard.service';

export const routes: Routes = [
    { path: '', redirectTo: 'overview', pathMatch: 'full' },
    {
        path: 'overview',
        loadChildren: './overview/overview.module#OverviewModule',
        canActivate: [AuthGuard],
    },
    { path: '**', component: NotFoundPageComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(
        routes
    )],
    exports: [RouterModule]
})
export class RoutesModule { }
