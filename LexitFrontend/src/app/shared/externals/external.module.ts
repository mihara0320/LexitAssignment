import { NgModule } from '@angular/core';

import { FlexLayoutModule } from '@angular/flex-layout';
import * as Hammer from 'hammerjs';

// custom modules
import { HighchartslModule } from './modules/highcharts.module';
import { MaterialModule } from './modules/material.module';

// external modules
import { ToastrModule } from 'ngx-toastr';

export const MODULES = [
    FlexLayoutModule,
    HighchartslModule,
    MaterialModule,
];

@NgModule({
    imports: MODULES,
    exports: MODULES,
})
export class ExternalModule {}
