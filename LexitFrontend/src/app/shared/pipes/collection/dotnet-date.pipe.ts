import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dotnetDate'
})
export class DotnetDatePipe implements PipeTransform {

  transform(dotnetDate: string, args?: any): any {
    if (!dotnetDate) { return; }
    const year = dotnetDate.substring(0, 4);
    const month = dotnetDate.substring(5, 7);
    const date = dotnetDate.substring(8, 10);

    return `${date}/${month}/${year}`;
  }

}
