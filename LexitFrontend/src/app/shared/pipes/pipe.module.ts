import { NgModule } from '@angular/core';

import { AntiCamelPipe } from './collection/anti-camel.pipe';
import { DotnetDatePipe } from './collection/dotnet-date.pipe';

export const PIPES = [
    AntiCamelPipe,
    DotnetDatePipe
];

@NgModule({
    declarations: PIPES,
    exports: PIPES,
})
export class PipesModule { }
