import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HTTP_INTERCEPTORS } from '@angular/common/http';

// Interceptors
import { TokenInterceptor } from '@shared/interceptors/collection/token.interceptor';
import { ErrorInterceptor } from '@shared/interceptors/collection/error.interceptor';

// External Modules
import { ExternalModule } from './externals/external.module';

// pipes
import { PipesModule } from './pipes/pipe.module';

// Components
import { ComponentsModule } from './components/components.module';


const MODULES = [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ExternalModule,
    PipesModule,
    ComponentsModule,
];

@NgModule({
    imports: MODULES,
    exports: MODULES,
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
                { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
            ]
        };
    }
}
