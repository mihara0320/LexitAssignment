import { Component, OnInit, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'smartphone-display',
  templateUrl: './smartphone-display.component.html',
  styleUrls: ['./smartphone-display.component.scss']
})
export class SmartphoneDisplayComponent implements OnInit {
  @Input() title: EventEmitter<string>;
  @Input() message: EventEmitter<string>;
  @Input() imageUrl: EventEmitter<string>;
  @Input() pending: EventEmitter<boolean>;
  @ViewChild('preview') preview: ElementRef;

  imageSrc: string;

  ngOnInit() {
    this.imageUrl.subscribe(url => {
      this.createPreview(url);
    });
  }
  createPreview(src) {
    const preview: HTMLImageElement = this.preview.nativeElement;
    preview.src = src;
    this.imageSrc = src;
  }
}
