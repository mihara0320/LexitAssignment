import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartphoneDisplayComponent } from './smartphone-display.component';

describe('SmartphoneDisplayComponent', () => {
  let component: SmartphoneDisplayComponent;
  let fixture: ComponentFixture<SmartphoneDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartphoneDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartphoneDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
