import { Component, OnInit, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'card-content',
  templateUrl: './card-content.component.html',
  styleUrls: ['./card-content.component.scss']
})
export class CardContentComponent implements OnInit {
  @Input() pending = new EventEmitter<boolean>();
  @Input() underflow = new EventEmitter<string>();

  isPending = true;
  underflowMessage: string = null;

  ngOnInit() {
    this.pending.subscribe(bool => this.isPending = bool);
    this.underflow.subscribe(message => this.underflowMessage = message);
  }

}
