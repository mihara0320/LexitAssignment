import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes,
  query,
  stagger
} from '@angular/animations';
import { Chart } from '@classes/Chart/class';

// format -> [timestamp, value]
export interface LineChartItem {
  [index: number]: number;
}

@Component({
  selector: 'line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss'],
  animations: [
    trigger('animState', [
      transition('* => in', [
        query('*', [
          style({ opacity: 0 }),
          animate('1000ms ease-in-out', style({ opacity: 1 }))
        ]),
      ])
    ])
  ]
})
export class LineChartComponent extends Chart implements OnInit {
  @Input() items: Observable<LineChartItem[]>;
  @Input() seriesName: string;

  ngOnInit() {
    const items$ = this.items.subscribe(items => {
      const option = this.buildOption(items);
      this.setOptions(option, () => this.animState = 'in');
    });
    this.subscriptions.push(items$);
  }
  buildOption(items: LineChartItem[]): any {
    return {
      chart: { redraw: false },
      rangeSelector: { enabled: false },
      navigator: { enabled: false },
      scrollbar: { enabled: false },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      exporting: {
        buttons: {
          contextButton: { enabled: false }
        }
      },
      series: [{
        name: this.seriesName,
        data: items
      }]
    };
  }

}
