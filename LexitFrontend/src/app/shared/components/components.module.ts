import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExternalModule } from '@shared/externals/external.module';
import { PipesModule } from '@shared/pipes/pipe.module';

import { SimpleMessageComponent } from '@shared/components/simple-message/simple-message.component';

import { CardComponent } from '@shared/components/card/card.component';
import { CardHeaderComponent } from '@shared/components/card-header/card-header.component';
import { CardContentComponent } from '@shared/components/card-content/card-content.component';
import { ListItemComponent } from '@shared/components/list-item/list-item.component';
import { OverlayComponent } from '@shared/components/overlay/overlay.component';

import { LineChartComponent } from '@shared/components/line-chart/line-chart.component';

import { ImageUploadButtonComponent } from '@shared/components/image-upload-button/image-upload-button.component';
import { SmartphoneDisplayComponent } from '@shared/components/smartphone-display/smartphone-display.component';


export const COMPONENTS = [
    SimpleMessageComponent,
    CardComponent,
    CardHeaderComponent,
    CardContentComponent,
    ListItemComponent,
    OverlayComponent,
    LineChartComponent,
    ImageUploadButtonComponent,
    SmartphoneDisplayComponent,
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ExternalModule,
        PipesModule,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class ComponentsModule { }
