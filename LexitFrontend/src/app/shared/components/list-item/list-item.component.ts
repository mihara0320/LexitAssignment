import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent  {
  @Input() imageUrl: string;
  @Input() name: string;
  @Input() hint: any;
  @Input() selected: boolean;
  @Output() enter = new EventEmitter<string>();
  @Output() leave = new EventEmitter();
}
