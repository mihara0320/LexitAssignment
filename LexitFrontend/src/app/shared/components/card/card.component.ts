import { Component, AfterContentInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes,
  query,
  stagger
} from '@angular/animations';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  animations: [
    trigger('animState', [
      transition('* => in', [
        query('*', [
          style({ opacity: 0, transform: 'scale(0)' }),
          animate('1000ms ease-in-out', style({ opacity: 1, transform: 'scale(1)' }))
        ]),
      ])
    ])
  ]

})
export class CardComponent implements AfterContentInit  {
  animState;

  ngAfterContentInit () {
    this.animState = 'in';
  }
}
