import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'image-upload-button',
  templateUrl: './image-upload-button.component.html',
  styleUrls: ['./image-upload-button.component.scss']
})
export class ImageUploadButtonComponent {
  @Output() fileSet = new EventEmitter<File>();
  @Output() fileReset = new EventEmitter<boolean>();
  @ViewChild('fileUploader') fileUploader: ElementRef;
  @ViewChild('imageValidiator') imageValidiator: ElementRef;

  imageFile: File;

  reset(): void {
    this.imageFile = null;
    this.fileUploader.nativeElement.value = null;
    this.fileReset.emit(true);
  }
  setFile(files: FileList) {
    const image = files[files.length - 1];
    this.imageFile = image;
    this.fileSet.emit(image);
  }
  openFileUploder() {
    this.fileUploader.nativeElement.click();
  }
}
