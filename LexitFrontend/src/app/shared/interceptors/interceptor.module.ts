import { NgModule } from '@angular/core';
import { TokenInterceptor } from './collection/token.interceptor';

@NgModule({
    // providers: [
    //     { provide: HTTP_INTERCEPTORS, useClass: ProgressInterceptor, multi: true, deps: [ProgressBarService] },
    //     { provide: HTTP_INTERCEPTORS, useClass: TimingInterceptor, multi: true },
    //     { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
    // ]
})
export class InterceptorModule {
}
