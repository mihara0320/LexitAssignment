import { HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from '@auth/services/auth.service';

import { Store } from '@ngrx/store';
import * as fromAuth from '@auth/reducers';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    messageTimeout: any;
    constructor(
        private store: Store<fromAuth.State>,
        private authService: AuthService,
        private toastr: ToastrService
    ) {

    }
    showMessage(message): void {
        if (this.messageTimeout) {
            clearTimeout(this.messageTimeout);
        }
        this.messageTimeout = setTimeout(() => {
            this.toastr.error(message, 'Error', { timeOut: 2000 });
        }, 300);
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
        return next
            .handle(req)
            .pipe(
                catchError((err: HttpErrorResponse) => {
                    console.log(err);
                    this.showMessage(err.error);
                    return throwError(err);
                })
            );
    }
}
