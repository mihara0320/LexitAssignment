import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api } from '@config/app.config';
import { Meter, Record, RegisterMeter, RegisterRecord } from '@core/models/meter';

@Injectable({
  providedIn: 'root'
})
export class MeterService {
  constructor(
    private _http: HttpClient,
  ) { }
  registerMeter(payload: RegisterMeter): Observable<Meter> {
    const endpoint = api.meter;
    return this._http
      .post<Meter>(endpoint, payload);
  }
  registerRecord(payload: RegisterRecord): Observable<Record> {
    const endpoint = api.record;
    return this._http
      .post<Record>(endpoint, payload);
  }
  getMeter(customerNumber: number): Observable<Meter> {
    const endpoint = `${api.meter}/${customerNumber}`;
    return this._http
      .get<Meter>(endpoint);
  }
  getRecords(meterId: number): Observable<Record[]> {
    const endpoint = `${api.record}/${meterId}`;
    return this._http
      .get<Record[]>(endpoint);
  }
}
