import { NgModule } from '@angular/core';
import { MeterService } from '@core/services/meter.service';


@NgModule({
    providers: [
        MeterService
    ]
})
export class ServicesModule {}
