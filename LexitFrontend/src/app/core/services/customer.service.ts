import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { api } from '@config/app.config';
import { Customer, Contract } from '@core/models/summary';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(
    private _http: HttpClient,
  ) { }
  getContract(customerNumber: number): Observable<Contract> {
    const endpoint = `${api.contract}/${customerNumber}`;
    return this._http
      .get<Contract>(endpoint);
  }
  updateCustomer(payload: Customer): Observable<Customer> {
    const endpoint = `${api.customer}/${payload.customerNumber}`;
    return this._http
      .put<Customer>(endpoint, payload);
  }
}
