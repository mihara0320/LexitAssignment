import { Customer } from '@core/models/summary';

export interface RegisterMeter {
    customerNumber?: number;
    registerType?: string;
    initialValue?: number;
}

export interface RegisterRecord {
    meterId: number;
    value: number;
}

export interface Meter {
    id?: number;
    customer?: Customer;
    customerId?: number;
    initialValue: number;
    installedDate?: string;
    lastUpdate?: string;
    registerType: string;
    records?: any[];
}

export interface Record {
    Id?: number;
    Value?: number;
    Meter?: Meter;
    MeterId?: number;
    CollectionDate?: string;
    LastUpdate?: string;
}
