export interface Customer {
    id?: number;
    customerNumber: number;
    firstName: string;
    lastName: string;
    address: string;
    email: string;
    lastUpdate: string;
}

export interface Contract {
    id: number;
    contractNumber: number;
    customerId?: number;
    customer?: Customer;
    expireDate: string;
    billingFrequency: number;
    isActive: boolean;
    lastUpdate: number;
}
