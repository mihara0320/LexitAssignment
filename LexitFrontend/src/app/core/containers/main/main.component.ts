import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { select, Store } from '@ngrx/store';
import * as AuthActions from '@auth/actions/auth.actions';
import * as fromAuth from '@auth/reducers';
import * as fromRoot from '../../../reducers';

import { Customer } from '@core/models/summary';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-root',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  subscriptions: Subscription[] = [];
  loggedIn$: Observable<boolean>;
  customer$: Observable<Customer>;
  customerId: number;
  email: string = null;

  constructor(
    private store: Store<fromRoot.State>,
    public dialog: MatDialog,
  ) {
    this.loggedIn$ = this.store.pipe(select(fromAuth.getLoggedIn));
    this.customer$ = this.store.pipe(select(fromAuth.getCustomer));
  }

  ngOnInit() {
    this.customer$.subscribe(customer => {
      if (customer) {
        this.email = customer.email;
      }
    });
  }

  logout() {
    this.store.dispatch(new AuthActions.Logout());
  }
}
