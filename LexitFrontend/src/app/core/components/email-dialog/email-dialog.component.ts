import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Dialog } from '@classes/Dialog/class';

@Component({
  selector: 'app-email-dialog',
  templateUrl: './email-dialog.component.html',
  styleUrls: ['./email-dialog.component.scss']
})
export class EmailDialogComponent extends Dialog implements OnInit {
  title: string;
  actionType: string;
  constructor(
    private fb: FormBuilder,
    public dRef: MatDialogRef<EmailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(fb, dRef);
    this.buildFormGroup({
      email: [null, [Validators.required, Validators.email]]
    });
  }
  ngOnInit(): void {
    this.title = this.data.title;
    this.actionType = this.data.actionType;
  }
}
