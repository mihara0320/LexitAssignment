import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';

import { ToolbarComponent } from './toolbar/toolbar.component';
import { TabLinkComponent } from './tab-link/tab-link.component';
import { MenuComponent } from './menu/menu.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { EmailDialogComponent } from '@core/components/email-dialog/email-dialog.component';


export const COMPONENTS = [
    ToolbarComponent,
    TabLinkComponent,
    MenuComponent,
    MenuItemComponent,
    EmailDialogComponent
];

@NgModule({
    imports: [
        SharedModule
    ],
    entryComponents: [
        EmailDialogComponent,
    ],
    declarations: COMPONENTS,
    exports: COMPONENTS,
})
export class ComponentsModule { }
