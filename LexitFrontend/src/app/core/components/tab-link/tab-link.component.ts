import { Component } from '@angular/core';
import { links } from '@config/app.config';
@Component({
  selector: 'app-tab-link',
  templateUrl: './tab-link.component.html',
  styleUrls: ['./tab-link.component.scss']
})
export class TabLinkComponent {
  public navLinks = links.activeLinks;
  constructor() { }
}
