import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@shared/shared.module';
import { ComponentsModule } from './components/components.module';
import { ServicesModule } from './services/services.module';

import { MainComponent } from './containers/main/main.component';
import { NotFoundPageComponent } from './containers/not-found-page/not-found-page.component';


export const CONTAINERS = [
    MainComponent,
    NotFoundPageComponent
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        SharedModule,
        ComponentsModule,
        ServicesModule
    ],
    declarations: CONTAINERS,
    exports: CONTAINERS,
})
export class CoreModule {
    static forRoot() {
        return {
            ngModule: CoreModule,
        };
    }
}
