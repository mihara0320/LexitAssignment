import { OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromAuth from '@auth/reducers';

import { Customer, Contract } from '@core/models/summary';

export class Page implements OnInit, OnDestroy {
    subscriptions: Subscription[] = [];
    animState: string;

    loggedIn$: Observable<boolean>;
    customer$: Observable<Customer>;
    page$: Observable<any>;

    constructor(public store: Store<fromRoot.State>) {
        this.loggedIn$ = this.store.pipe(select(fromAuth.getLoggedIn));
        this.customer$ = this.store.pipe(select(fromAuth.getCustomer));
    }

    ngOnInit() {

    }

    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
