import { Inject, OnInit } from '@angular/core';
import {
    FormBuilder,
    FormGroup,
    FormControl,
    AbstractControl,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material';


export class Form implements OnInit {
    public formBuilder: FormBuilder;
    public dialogRef: MatDialogRef<any>;
    public formGroup: FormGroup;
    constructor(formBuilder) {
        this.formBuilder = formBuilder;
    }

    ngOnInit() {
        this.formGroup.updateValueAndValidity();
    }

    buildFormGroup(controlsConfig, controlsOption?) {
        this.formGroup = controlsOption
            ? this.formBuilder.group(controlsConfig, controlsOption)
            : this.formBuilder.group(controlsConfig);
    }
    getFormControl(target): AbstractControl {
        return this.formGroup.controls[target];
    }
    isValid(): boolean {
        return this.formGroup.valid;
    }
    isValidAsync(): Promise<boolean> {
        this.formGroup.updateValueAndValidity();
        return new Promise(reslove => {
            return reslove(this.formGroup.valid);
        });
    }
}
