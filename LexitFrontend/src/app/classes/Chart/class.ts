import { OnInit, Input, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';

export class Chart implements OnDestroy {
    subscriptions: Subscription[] = [];
    animState: string;
    options: any;

    setOptions(options, callback?) {
        this.options = options;
        if (callback) { callback(); }
    }
    ngOnDestroy(): void {
        this.subscriptions.forEach(s => s.unsubscribe());
    }
}
