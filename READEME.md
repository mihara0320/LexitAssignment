# Meter Reporting App

## Introduction

This app is created for a test assignment. <br />
User can login to the application with 3 predefined user account: <br />
 <br />
<< Mike Tyson >> <br />
CustomerNumber: 56392 <br />
ContractNumber: 3197063 <br />
 <br />
<< John Lennon >> <br />
CustomerNumber: 85134<br />
ContractNumber: 9857458<br />
 <br />
<< Michel Jackson >> <br />
CustomerNumber: 79634<br />
ContractNumber: 7845241<br />
 <br />
Where Mike is a user who has been using this app for a while, John is just got registered, and Michel is no longer has valid contract. 

## Run Application
###Server-side 
<br>
* Update-Database <br>
* Start running server <br>

###Client-side 
<br>
* npm i <br>
* npm run build <br>
* npm run start <br> 